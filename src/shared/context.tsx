import React, { createContext, useContext, useEffect, useState } from 'react'
import axios from "axios";

const API_KEY = '1b0691338e8fbefc1c16321a7e6f6260'
const API_URL = `https://api.openweathermap.org/data/2.5/forecast?&appid=${API_KEY}&units=metric&cnt=6&lang=ru`

const GlobalContext = createContext<GlobalController>({} as GlobalController)

interface Props {
    children: any;
}

const GlobalContextProvider = (props: Props) => {
    const [data, setData] = useState<GlobalState>({
        weatherData: null,
        loading: true,
        activeWeatherItemId: 0,
        activeWeatherItem: null,
        cityTitle: 'Kyiv'
    })

    useEffect(() => {
        // fetchWeatherData()
        axiosWeatherData()
    }, [data.cityTitle])

    const axiosWeatherData = () => {
        axios.get<any>(API_URL, {params: {q: data.cityTitle}})
            .then(res => {
                if (res !== null) {
                    setData(prevState => {
                        return {
                            ...prevState,
                            activeWeatherItemId: res.data.list[0].dt,
                            weatherData: {
                                id: res.data.city.id,
                                cityName: res.data.city.name,
                                weatherList: res.data.list
                            }
                        }
                    })
                }
            })
            .catch((error) => console.log('error', error.response.data.message))
    }

    const fetchWeatherData = async () => {
        setLoading(true)
        const data = await fetch(API_URL)
        const items = await data.json()
        setData(prevState => {
            return {
                ...prevState,
                activeWeatherItemId: items.list[0].dt,
                weatherData: {
                    id: items.city.id,
                    cityName: items.city.name,
                    weatherList: items.list
                }
            }
        })
        setLoading(false)
    }

    const changeCityName = (cityName: string) => {
        setData(prevState => {
            return {
                ...prevState,
                cityTitle: cityName
            }
        })
    }

    const setActiveWeatherItemId = (id: number) => {
        setData(prevState => {
            return {
                ...prevState,
                activeWeatherItemId: id
            }
        })
    }


    const setLoading = (value: boolean) => {
        setData(prevState => {
            return {
                ...prevState,
                loading: value
            }
        })
    }

    const getAllWeatherData = (): any => {
        return data.weatherData
    }

    const getWeatherItemById = (id: number): WeatherItemInfo | undefined => {
        return data.weatherData?.weatherList?.find((item: WeatherItemInfo) => item.dt === id)
    }


    const globalState: GlobalController = {
        state: data,
        getAllWeatherData,
        setActiveWeatherItemId,
        getWeatherItemById,
        changeCityName
    }

    return (
        <GlobalContext.Provider value={globalState}>
            {props.children}
        </GlobalContext.Provider>
    )
}

type Optional<T> = T | null | undefined

export interface GlobalController {
    state: GlobalState
    getAllWeatherData: () => WeatherObjectData | null
    setActiveWeatherItemId: (id: number) => void
    getWeatherItemById: (id: number) => Optional<WeatherItemInfo>
    changeCityName: (cityName: string) => void
}


export interface GlobalState {
    weatherData: WeatherObjectData | null
    loading: boolean
    activeWeatherItemId: number
    activeWeatherItem: Optional<WeatherItemInfo>
    cityTitle: string
}

export interface WeatherObjectData {
    id: number
    cityName: string
    weatherList: WeatherItemInfo[]
}

export interface WeatherItemInfo {
    dt: number
    dt_txt: string
    main: {
        temp: number
        feels_like: number
        pressure: number
        humidity: number
    }
    wind: {
        speed: number
    }
    weather: [
        {
            description: string
            icon: string
        }
    ]
}

export default GlobalContextProvider

export const useGlobalContext = () => useContext<GlobalController>(GlobalContext)