import React from "react";
import {useGlobalContext, WeatherItemInfo} from "../shared/context";
import {WeatherItem} from "./WeatherItem";


export const WeatherList = () => {
    const {state} = useGlobalContext()
    // console.log('all weather data', state.weatherData)
    return (
        <div className="weather-list">
            {
                state.weatherData
                && state.weatherData.weatherList
                    .map((item: WeatherItemInfo) =>
                        <WeatherItem weatherItemData={item} key={item.dt} />)
            }
        </div>
    )
}