import React, {useState} from "react";
import {useGlobalContext, WeatherItemInfo} from "../shared/context";
import {Transition} from 'react-transition-group'
interface Props {
    weatherItemData: WeatherItemInfo
}

export const WeatherItem = (props: Props) => {
    const {weatherItemData} = props
    const {setActiveWeatherItemId, state} = useGlobalContext()
    const activeClass = state.activeWeatherItemId === weatherItemData.dt ? 'active' : ''
    // console.log('weather item data', weatherItemData)

    return (
        <div
            className={['weather-item', activeClass].join(' ')}
            onClick={() => setActiveWeatherItemId(weatherItemData.dt)}>
            <div className="weather-item-date">{weatherItemData.dt_txt}</div>
            <div className="weather-item-temp">{weatherItemData.main.temp}</div>
            <div className="weather-item-feels-like">{weatherItemData.main.feels_like}</div>
        </div>
    )
}