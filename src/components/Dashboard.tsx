import React from "react";
import {useGlobalContext} from "../shared/context";

export const Dashboard = () => {
    const {state, getWeatherItemById, changeCityName} = useGlobalContext()
    const weatherItemData = getWeatherItemById(state.activeWeatherItemId)
    const iconUrl = `http://openweathermap.org/img/w/${weatherItemData?.weather[0].icon}.png`

    // console.log('[dashboard] weather item data', weatherItemData)

    if (!weatherItemData) {
        return (
            <div>empty</div>
        )
    }

    const onSubmit = (event: any) => {
        event.preventDefault()
        changeCityName(event.target[0].value)
    }

    return (
        <div className="weather-details-holder">
            <div className="weather-details-main-info">
                <div style={{width: '100px', height: '100px', position: 'relative'}}>
                    <img src={iconUrl} alt="" className={"dashboard-icon"}/>
                </div>
                <div className="city-name">{state.weatherData?.cityName}</div>
                <div className="temp">{weatherItemData.main.temp}</div>
            </div>
            <form onSubmit={onSubmit}>
                <input type="text" />
            </form>
            <div className="weather-details-secondary-info">
                <div className="humidity">{weatherItemData.main.humidity} %</div>
                <div className="air-pressure">{weatherItemData.main.pressure} PS</div>
                <div className="wind-speed">{weatherItemData.wind.speed} km/h</div>
            </div>
        </div>
    )
}