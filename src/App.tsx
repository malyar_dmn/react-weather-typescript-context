import React from 'react';
import './App.css'
import {WeatherList} from "./components/WeatherList";
import {Dashboard} from "./components/Dashboard";

function App() {
    return (
        <div className="container">
            <Dashboard />
            <WeatherList />
        </div>
    )
}

export default App;
